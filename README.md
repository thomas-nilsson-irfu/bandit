# Bandit analyzer

This analyzer is a wrapper around [Bandit](https://github.com/PyCQA/bandit),
a security linter for Python source code.
It's written in Go using
the [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
shared by all analyzers.

The [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
contains documentation on how to run, test and modify this analyzer.

## Which Python version to use?

As noted in the [project's home page](https://pypi.org/project/bandit/), the version 
of Python to be used is dependent upon what is being scanned. Python 2 is end of life, 
so we're using Python 3. However, this analyzer currently needs to use Python 3.7 
rather than Python 3.8. When updating to Python 3.8, this analyzer finds less than it 
does with Python 3.7. [This has been noted as an issue](https://github.com/PyCQA/bandit/issues/639) 
in the project. 

## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## License

This code is distributed under the MIT Expat license, see the [LICENSE](LICENSE) file.
