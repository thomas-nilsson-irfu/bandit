# Bandit analyzer changelog

## v2.10.0
- Update common to v2.22.0 (!60)
- Update urfave/cli to v2.3.0 (!60)

## v2.9.6
- Update common to version v2.21.3 (!59)
-
## v2.9.5
- Update common and enable disablement of custom rulesets (!58)

## v2.9.4
- Update golang dependencies (!57)

## v2.9.3
- Update finding links to new location (!52)

## v2.9.2
- Update golang dependencies (!50)

## v2.9.1
- Update golang dependencies (!48)
- Add documentation about which Python version to use (!48)

## v2.9.0
- Add `scan.start_time`, `scan.end_time` and `scan.status` to report (!46)

## v2.8.0
- Switch base image to alpine (!45)

## v2.7.2
- Upgrade go to version 1.15 (!44)

## v2.7.1
- Log error when failing to create a code fingerprint (!43)

## v2.7.0
- Add scan object to report (!40)

## v2.6.0
- Switch to the MIT Expat license (!37)

## v2.5.0
- Update logging to be standardized across analyzers (!36)

## v2.4.1
- Use a Slim base image (!28)
- Remove `location.dependency` from the generated SAST report (!33)

## v2.4.0
- Add `id` field to vulnerabilities in JSON report (!20)

## v2.3.0
- Add support for custom CA certs (!17)

## v2.2.2
- Use Debian Stretch as base image (!14)

## v2.2.1
- Update bandit version to 1.6.2

## v2.2.0
- Add `SAST_BANDIT_EXCLUDED_PATHS` option to exclude paths from scan
- Add `SAST_EXCLUDED_PATHS` option to exclude paths from report (common v2.3.0)
- Report "confirmed" confidence instead of "critical" (common v2.2.0)

## v2.1.1
- Update common to v2.1.6

## v2.1.0
- Update bandit version to 1.5.1

## v2.0.0
- Switch to new report syntax with `version` field

## v1.2.0
- Add `Scanner` property and deprecate `Tool`

## v1.1.0
- Enrich report with more data

## v1.0.0
- initial release
