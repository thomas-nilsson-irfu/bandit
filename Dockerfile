FROM golang:1.15-alpine AS build
ENV CGO_ENABLED=0 GOOS=linux
WORKDIR /go/src/app
COPY . .
# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
        PATH_TO_MODULE=`go list -m` && \
        go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o analyzer

FROM python:3.7-alpine

RUN apk add --no-cache git

ARG SCANNER_VERSION
ENV SCANNER_VERSION ${SCANNER_VERSION:-1.6.2}

RUN pip install bandit==$SCANNER_VERSION

# Install analyzer
COPY --from=build --chown=root:root /go/src/app/analyzer /analyzer

ENTRYPOINT []
CMD ["/analyzer", "run"]
