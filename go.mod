module gitlab.com/gitlab-org/security-products/analyzers/bandit/v2

require (
	github.com/sirupsen/logrus v1.7.0
	github.com/urfave/cli/v2 v2.3.0
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.22.0
)

go 1.15
