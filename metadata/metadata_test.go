package metadata_test

import (
	"reflect"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/bandit/v2/metadata"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

func TestReportScanner(t *testing.T) {
	want := issue.ScannerDetails{
		ID:      "bandit",
		Name:    "Bandit",
		Version: metadata.ScannerVersion,
		Vendor: issue.Vendor{
			Name: "GitLab",
		},
		URL: "https://github.com/PyCQA/bandit",
	}
	got := metadata.ReportScanner

	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}
